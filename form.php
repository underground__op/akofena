<?php include 'header.php';?>
<!-- reservation-information -->
<div id="information" class="spacer reserve-info ">
<div class="container">
<div class="row">
<div class="col-sm-7 col-md-8">
    <div class="embed-responsive embed-responsive-16by9 wowload fadeInLeft"><iframe width="560" height="315" src="https://www.youtube.com/embed/PMivT7MJ41M" frameborder="0" allowfullscreen></iframe></div>
</div>
<div class="col-sm-5 col-md-4">
<h3>Reservation</h3>
           <form role="form" class="wowload fadeInRight" autocomplete="off"
	enctype="multipart/form-data" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" onSubmit="return validate(this);" name="form">
	
        <div class="form-group">
            <input type="text" class="form-control"  placeholder="Name" name="name">
        </div>
        <div class="form-group">
            <input type="email" class="form-control"  placeholder="Email" name="email">
        </div>
        <div class="form-group">
            <input type="Phone" class="form-control"  placeholder="Phone" name="phno">
        </div> 
		<div class="form-group">
		
              <select class="form-control col-sm-2" name="room" >
                <option value="00">RoomNo.</option>
                <option value="01">1</option>
                <option value="02">2</option>
                <option value="03">3</option>
                <option value="04">4</option>
                <option value="05">5</option>
                <option value="06">6</option>
                <option value="07">7</option>
                <option value="08">8</option>
                <option value="09">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
            
 		 </div> 
        
        <div class="form-group">
            <div class="row">
			  
            <div class="col-xs-4">
            <select class="form-control col-sm-2" name="day" >
			   <option value="00">day</option>
                <option value="01">1</option>
                <option value="02">2</option>
                <option value="03">3</option>
                <option value="04">4</option>
				 <option value="05">5</option>
                <option value="06">6</option>
                <option value="07">7</option>
                <option value="08">8</option>
				 <option value="09">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
				 <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
				<option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
				 <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
				 <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
				 <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
                
                
              </select>
            </div>
            <div class="col-xs-4">
              <select class="form-control col-sm-2" name="month" >
                 <option value="00">Month</option>
                <option value="01">Jan (01)</option>
                <option value="02">Feb (02)</option>
                <option value="03">Mar (03)</option>
                <option value="04">Apr (04)</option>
                <option value="05">May (05)</option>
                <option value="06">June (06)</option>
                <option value="07">July (07)</option>
                <option value="08">Aug (08)</option>
                <option value="09">Sep (09)</option>
                <option value="10">Oct (10)</option>
                <option value="11">Nov (11)</option>
                <option value="12">Dec (12)</option>
              </select>
            </div>
            <div class="col-xs-4">
              <select class="form-control" name="year">
			   <option value="00">year</option>
                <option value="13">2013</option>
                <option value="14">2014</option>
                <option value="15">2015</option>
                <option value="16">2016</option>
                <option value="17">2017</option>
                <option value="18">2018</option>
                <option value="19">2019</option>
                <option value="20">2020</option>
                <option value="21">2021</option>
                <option value="22">2022</option>
                <option value="23">2023</option>
              </select>
            </div>
          </div>
        </div>
		<div class="form-group">
            <textarea class="form-control"  placeholder="Message(Optional)" rows="4" name="message"></textarea>
        </div>
		<h2>Payments<h2>
		<div class="form-group">
            <input type="text" class="form-control"  placeholder="Credit Card Name" name="namecard">
        </div> 
		<div class="form-group">
            <input type="text" class="form-control"  placeholder="Credit Card Number" name="numbercard">
        </div>   
 <div class="form-group">
            <input type="text" class="form-control"  placeholder="Pin" name="pin">
        </div>  
        
        <button id="myBtn" class="btn btn-default" >

		Book & Pay </button>
    </form> 

 <?php
      // define variables and set to empty values
      $day = $month = $year = $cardname = $cardnumber = "";
      $name = $email = $phone = $room = $message = $pin ="";
	  
	    $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "hotel_info";
        $sql = "SELECT * FROM MyGuests";
		
		 

      // checks if method is POST and if input is empty
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
          $nameErr = "Name is required"; # note the assignment if input is empty 
        } else {
          // check if name only contains letters and whitespace
            if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
               $nameErr = "Only letters and white space allowed"; 
            } else{
                $name = test_input($_POST["name"]);
            }
        }
        
        if (empty($_POST["email"])) {
          $emailErr = "Email is required";
        } /**else {
          // check if e-mail address is well-formed
          if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format"; 
          }**/ else{
              $email = test_input($_POST["email"]);
          }
		
	  
	      
		   $day = test_input($_POST["day"]);
		   $month = test_input($_POST["month"]);
		   $year = test_input($_POST["year"]);
		   $cardname = test_input($_POST["namecard"]);
		   $cardnumber = test_input($_POST["numbercard"]);
		   $phone = test_input($_POST["phno"]);
		   $room = test_input($_POST["room"]);
		   $message = test_input($_POST["message"]);
		   $pin = test_input($_POST["pin"]);
		   
		  
       // check for email format later }
          
   /*     if (empty($_POST["website"])) {
          $website = "";
        } else {
          // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
          if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
            $websiteErr = "Invalid URL"; 
          } else{
              $website = test_input($_POST["website"]);
          }
        }*/

     /**   if (empty($_POST["comment"])) {
          $comment = "";
        } else {
          $comment = test_input($_POST["comment"]);
        }

        if (empty($_POST["gender"])) {
          $genderErr = "Gender is required";
        } else {
          $gender = test_input($_POST["gender"]);
        }**/
      } 

      function test_input($data) {
        $data = trim($data); # removes whitespaces before and after strings
        $data = stripslashes($data); # removes slashes from database or html inputs
        $data = htmlspecialchars($data); # guards against characters/symbols injection attacks
        return $data;
      }
	  
	/*  $a= $name ;
	  $b= $email ; */
	  
	  $a= 'car' ;
	  $b= 'd@f' ;
	  
	  // connection
	  if($email!=""){
           try {
	  echo $a;
			   echo "<br>";
	  echo $b;
				echo "<br>";
	 
		 
	   
                    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username,  $password);
                    // set the PDO error mode to exception
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					
					//cant inset varibles or not dong it correctly
					$sql ="INSERT INTO Book (name,email,phone,room,day,month,year,message,cname,cnumber,pin,reg_date)
					VALUES ('$name','$email','$phone','$room','$day','$month','$year','$message','$cardname','$cardnumber','$pin',null)";
					
					

                /* $sql = "CREATE TABLE Book (
					name VARCHAR(50),
                    email VARCHAR(50),
					phone INT(11),
					room  INT(2),
					day   INT(2),
					month INT(2),
					year  INT(2),
					message VARCHAR(150),
					cname VARCHAR(50),
					cnumber INT(17), 
					pin INT(4),
					reg_date TIMESTAMP
                    )"; */


                    // use exec() because no results are returned
                    $conn->exec($sql);
                    echo "Insert sucessful";
					
					
					
			

               } 
           catch(PDOException $e)
               {
                    if($sql == "SELECT * FROM MyGuests"){
                         echo "Connection was not established. Find reason below:<br><br>";
                         echo $sql . "<br>" . $e->getMessage();
                    }
                    
               }
			   $URL="http://localhost/sample/congrats.php";
echo "<script type='text/javascript'>window.location.href='{$URL}';</script>";
echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
	  }
	  else{
		   echo "<br>";
		  echo "fill the form :)";
	  }

			
            $conn = null;
				
		   
		   //dont worry about reloading the page/...let it move to next page
    ?>
<!--	
<script type="text/javascript">
window.location.href = 'http://localhost/sample/congrats.php';
</script>-->
</div>
</div>  
</div>
</div>
<!-- reservation-information -->

<?php include 'footer.php';?>
